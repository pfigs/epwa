from distutils.core import setup

setup(
    name="messaqe_agent",
    version="0.1",
    description="Simple message subscriber",
    author="Pedro Silva",
    entry_points={
        "console_scripts": ["nev_msg_agent=message_agent.__main__:mqtt_agent"]
    },
)
