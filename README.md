# epwa

<!-- MarkdownTOC -->

1. [Assignment](#assignment)
1. [Solution](#solution)
    1. [Introduction](#introduction)
    1. [MQTT API](#mqtt-api)
    1. [Message subscription agent](#message-subscription-agent)
    1. [Engaging with local service daemon](#engaging-with-local-service-daemon)
1. [Installation](#installation)
1. [Tests](#tests)
1. [Building and running](#building-and-running)
1. [Final considerations](#final-considerations)

<!-- /MarkdownTOC -->

We have a fleet of internet-enabled, camera-equipped devices. These devices are
powered on at the appropriate location, and should be automatically configured
from a cloud-based system, based on customer needs at that given time.
Upon booting, a device’s configuration may look something like the following
JSON structure:

```bash
{
    "id": "c93e2345-aa4c-4092-9efb-4fab84589cc6",
    "name": "R2-D2",
    "flags": 123,
    "schedules":
    {
        "2fa418f4-3549-41bc-88a8-ed4cbfcd9e20":
        {
            "flags": 456,
            "begin": "2020-01-05T14:15:00.000Z",
            "end": "2020-01-08T14:15:00.000Z"
        },
        "57f4a565-3f39-4601-a7ac-a49016c67d76":
        {
            "flags": 789,
            "begin": "2020-01-10T14:15:00.000Z",
            "end": "2020-01-15T14:15:00.000Z"
        }
    }
}
```

The most important property here is schedules: it defines when the device
should automatically start (and stop) recording video with its camera.
As properties in this configuration, the device may need to respond accordingly.

For example, if a schedule is removed from the schedules object, the device
should remove any systemd timers it may have created related to that schedule.
If a flag changes, the device may need to change its behavior in some way, such
as whether it automatically uploads the files it creates during a schedule.

## Assignment

Write an application that can read this JSON format and detect changes as
they occur. You are welcome to host this file on a server and implement change management however you wish (maybe you want to use an existing technology
such as MQTT or REST).

As these properties change, print a meaningful message to the screen (in lieu
of executing the relevant system command). Consider writing automated tests
which verify the intended behavior.

Be creative and find a way to show that you understand the problem domain and
can find a solution while keeping in mind that this is only a simple assignment.

For bonus points, describe or implement how you would communicate edge
state (e.g. a camera failure) back to the cloud.

## Solution

### Introduction

In the solution for this assignment, I have decided to pick a few
technologies I am mostly familiar with, Python and MQTT.

Python is a good language to quickly hack things together and for this
purpose it is a sensible pick as it is easy to engage with MQTT
and JSON streams.

I picked MQTT as it one of the major offers the cloud vendors provide in
order to connect edge devices to the cloud and allow
message passing between cloud to device (and vice versa).

### MQTT API

For the purpose of the demo I've defined a simple MQTT interface.

The interface consists of three distinct topics, one for configuration
messages, one for downstream messages
(cloud to edge) and another one for events,
such as loss of connection by the edge device (edge to cloud).

The topics are arranged as follows:

-   device/configuration/<id\>
-   device/message/<id\>
-   device/event/<id\>

where the device id is added as a means to distinguish between edge devices.

In the first topic, device/configuration, the cloud services
can publish a new JSON configuration for the edge device, such
as a new schedule.

In the second topic, device/message/, the edge device publishes
a request, notification that affects the device's runtime, such
as a shutdown request.

In the third topic, device/event/, the edge device publishes
information that is relevant to share with the cloud services, such
as a fault exception, a recording started, among others.

In this simple example, the payload is encoded in plain text JSON.
For bandwidth constrained environment, like a 2G cellular connection,
it would be good to reduce traffic (and bill) by resorting to a
binary format, eg, CBOR, protocol buffers.

When packing the data in binary format it is likely necessary
to keep the data syntax forward compatible, such as not
changing the meaning of the ids. It also raises the need
to share a definition of the content, such as a CDDL or protocol
buffer file.

CBOR is often an exception, as one can still have full text keys
in the binary stream, making a good compromise between
readability and bandwidth optimization.

Often, JSON is a requirement to use certain cloud vendors IOT/MQTT
solutions.

### Message subscription agent

The message subscription agent or, for short, message service
is a simple client that connects to a remote server in
order to obtain updates to its state.

The implementation is done is Python and I started by dribbling a
small flow diagram and class diagram.

![class diagram][class_diagram]

The class Device represents the edge device which will have a
default or locally preserved state
provided as an input argument (--state state.json (default)) and the
connection details as a second input argument
(--connection settings.json (default)). The Device class is
responsible for defining a connection type, eg, MQTT, an
initialization message, a disconnect message and how to
handle incoming messages. The messages are acquired from
a queue which is populated by the Connection class.

The Connection class contains a simple interface whose
goal is to support different protocols. In this example
I have only defined a MQTT specialization class. The
Connection class contains queues to handle incoming
request to/from the device and other queues to
handle outbound messages or events to/from the device.

The MQTT class contains the specialized code to interact
with a MQTT broker. It describes the topic API and which
callback to call when a message is received on that particular
pattern.

There are also a few other auxiliary classes, such as the StateFlags
and the Notify classes. The StateFlags describes which flags are
valid and the Notify class defines how the program would interact
externally with a system daemon, eg, systemd.

### Engaging with local service daemon

When it comes to engagement with systemd the scheduling of events has
to be done through timer service files. The timer service files
would need to be created by the application and then started through
the system daemon control. All of this could be done through Python,
assuming the program would have permissions to write to the systemd
paths.

An alternative would be to implement the watchdog and rely on an
alternative activation of the system units, for example, dbus. The
watchdog would be a fairly simple program, which would wait
for a schedule request, book it to the future and wait for any
further requests to come in.

While such approach would make the system more portable,
the dependency on systemd is still quite heavy and
there are other interesting alternatives for IPC, such
as ZeroMQ, gRPC, plain sockets, among others.

## Installation

Installing the package in this repository requires that you have
pip and Python installed in your environment.

For convenience, please use a virtual environment, such as virtualenv
or pyenv if you plan to run the software natively.

To make the installation, clone this repository and run:

```bash
    pip install .
```

If you plan to develop and submit a pull requests, it is important
to first install all the developer requirements,

```bash
    pip install -r dev-requirements.txt
```

and then install the git hooks and the package in development mode

```bash
    pre-commit install  # see .pre-commit-config.yaml
    pip install -e .
```

## Tests

Package tests are meant to be executed with pytest (from the root
of the repository) with:

```bash
    pytest -s
```

Alternatively you can test through a daemon docker service. A helper
file is available from:

```bash
    ./tests/service_tests.sh
```

## Building and running

Please use container/docker-compose.yml to build the application container:

```bash
    docker-compose -f container/docker-compose.yml build
```

When running you need to set the environment parameters with the correct
username, password and host destination. For the sake of the example
please use the unencrypted port (1883).

As an example, you can connect the service agent against the public MQTT
test broker with:

```bash
    export MQTT_HOST=test.mosquitto.org
    export MQTT_PORT=1883
    docker-compose -f container/docker-compose.yml up -d
```
## Example

[Execution example](https://gitlab.com/pfigs/epwa/raw/master/img/execution.gif)

## Final considerations

The package tests could be further extended by reading test data from
a JSON file, feed it to MQTT and assert that the changes happen as
described in the file. Nevertheless, most of the bindings are currently
in place and it would easy to extend the tests for such purpose.

The package is also written with Sphinx in mind, which would provide
the API documentation in several different formats, such as HTML, PDF,
among others.

For simplification, I assume the agent will exit in failure in case
the MQTT connection is not started. Thus, it becomes part of the host
system to manage the restart policy of the service (systemd, docker, or
any other system in use).

I also assume that the content is always meant to be parsed and applied.
A good improvement would be to add a sequence number or a simple
hash to avoid processing the whole change.

The code in this registry aims to illustrate my capabilities when it comes
to Python, packaging and delivering portable code. I recognize that
I could have taken the complexity a bit down by making a simpler
class or script that handles the mqtt connection (see tests/viewer.py).

[class_diagram]: ./img/class_diagram.png

