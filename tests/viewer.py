"""

    MQTT module
    ===========

    This module contains the MQTT specific interfaces and methods.

"""
import argparse
import json
import paho.mqtt.client as paho_mqtt


def on_connect(client, userdata, flags, rc):
    """
    Callback that is called when connection to MQTT has succeeded.
    Here, we're subscribing to the incoming topics.
    """
    print(rc)
    if rc == 0:
        print(f"Subscribing topic: {ARGS.topic}")
        client.subscribe(ARGS.topic, 1)


def on_publish(client, userdata, mid):
    """
    Callback that is called when connection to MQTT has succeeded.
    Here, we're subscribing to the incoming topics.
    """
    print("mid:", mid)


def on_message(client, userdata, message):
    """
    Callback that is called when connection to MQTT has succeeded.
    Here, we're subscribing to the incoming topics.
    """
    print("message:", message.payload)


def main():

    with open(ARGS.connection, "r") as inputfile:
        settings = json.load(inputfile)

    client = paho_mqtt.Client()

    client.on_connect = on_connect
    client.on_publish = on_publish
    client.on_message = on_message

    client.username_pw_set(settings["mqtt"]["username"], settings["mqtt"]["password"])
    client.connect(host=settings["mqtt"]["host"], port=settings["mqtt"]["port"])
    client.loop_forever()


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(description="Device configuration")
    PARSER.add_argument(
        "--connection",
        type=str,
        required=False,
        default="settings.json",
        help="path to connection settings",
    )
    PARSER.add_argument(
        "--topic", type=str, required=False, default="#", help="topic to subscribe to"
    )

    ARGS = PARSER.parse_args()
    main()
