"""
    Test MQTT

    This test file contains tests regarding the MQTT protocol
"""

import json
import message_agent
from message_agent.publisher import Publisher
from message_agent.flags import StateFlags
import context


def test_publish_data(records=None):
    """ Takes a list of payloads and topics and publishes them """

    settings = message_agent.utils.read_jfile(context.MQTT_SETTINGS_PATH)
    state = message_agent.utils.read_jfile(context.DEVICE_STATE_PATH)

    publisher = Publisher(settings)
    publisher.start(block=False)

    identifier = state["id"]
    records = dict()

    for _ in range(0, context.RANDOM_DATA_GENERATION):
        data = message_agent.utils.generate_data(identifier, flags=[StateFlags.RECORD])
        topic = f"device/configuration/{identifier}"
        records[identifier] = dict(topic=topic, data=data)

    feedback_publish_data(publisher, records, flags=StateFlags.RECORD.value)
    feedback_publish_data(publisher, records, flags=StateFlags.STOP.value)
    feedback_publish_data(publisher, records, flags=StateFlags.UPLOAD.value)
    shutdown_request(publisher, identifier)
    publisher.shutdown()
    return records


def shutdown_request(publisher, identifier):
    """ Creates an send a shutdown request """
    data = dict()
    data["type"] = {StateFlags.OFFLINE.name: StateFlags.OFFLINE.value}
    data["message"] = f"exit request"
    topic = f"device/message/{identifier}"
    print(json.dumps(data, sort_keys=True, indent=4))
    publisher.publish(topic, json.dumps(data), qos=0).wait_for_publish()
    publisher.loop()


def feedback_publish_data(publisher, records, flags=None):
    """ Takes a list of payloads and topics and publishes them """
    for _, message in records.items():
        data = message["data"]
        topic = message["topic"]
        print(f"feedback data to: {topic}")

        if flags:
            data["flags"] = flags

        print(json.dumps(data, sort_keys=True, indent=4))
        info = publisher.publish(topic, json.dumps(data), qos=2)
        info.wait_for_publish()
        print(info)


def test_generate_data():
    """ Ensure that data can be generated and that the identifier can be specified """
    state = message_agent.utils.read_jfile(context.DEVICE_STATE_PATH)
    identifier = state["id"]
    for _ in range(0, context.RANDOM_DATA_GENERATION):
        data = message_agent.utils.generate_data(identifier)

    for _ in range(0, context.RANDOM_DATA_GENERATION):
        data = message_agent.utils.generate_data()
        if data["id"] == identifier:
            raise ValueError("Incorrect generation of data. Id should be different")


if __name__ == "__main__":
    test_publish_data()
