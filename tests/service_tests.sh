#!/usr/bin/env bash
# Builds and stats nev as a deamon where tests
# are executed afterwards

set -e

_tests()
{
    # package tests
    docker exec -it nev pytest -s
    docker logs nev

    # Other tests
}

_main()
{
    docker-compose -f tests/docker-compose.yml down || true
    docker-compose -f tests/docker-compose.yml up -d --build

    _tests

    docker-compose -f tests/docker-compose.yml down || true
}

_main
