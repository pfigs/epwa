"""
    Test Interfaces

    This test file contains tests regarding the instantiation
    of the package classes
"""
import datetime
import message_agent
import context


def test_interface_import():
    """ Tests the DEVICE class instantiation """

    settings = message_agent.utils.read_jfile(context.MQTT_SETTINGS_PATH)
    state = message_agent.utils.read_jfile(context.DEVICE_STATE_PATH)

    device = message_agent.Device(state=state, settings=settings)
    return device


def test_schedule():
    """ Ensure the creation of a Schedule object works properly """
    state = message_agent.utils.read_jfile(context.DEVICE_STATE_PATH)

    identifier = None
    content = None
    for identifier, content in state["schedules"].items():
        obj = message_agent.Schedule(identifier, content)

    # assert the content of the the last object match the input
    if obj.identifier != identifier:
        raise ValueError(
            f"Schedule identifier is incorrect: {obj.identifier} != {identifier}"
        )

    if obj.flags != content["flags"]:
        raise ValueError(
            f'Schedule flags is incorrect: {obj.flags.value} != {content["flags"]}'
        )

    begin = datetime.datetime.fromisoformat(content["begin"].replace("Z", ""))
    end = datetime.datetime.fromisoformat(content["end"].replace("Z", ""))

    if obj.begin != begin:
        raise ValueError(f"Schedule begin is incorrect: {obj.begin} != {begin}")

    if obj.end != end:
        raise ValueError(f"Schedule end is incorrect: {obj.end} != {end}")


def start_mqtt():
    """ Starts the DEVICE's MQTT interface """

    device = test_interface_import()
    device.start()


if __name__ == "__main__":
    test_schedule()
