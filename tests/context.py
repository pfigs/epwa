"""
    Test context

    Defines common methods and parameters for the other
    test scripts

"""

MQTT_SETTINGS_PATH = "settings.json"
DEVICE_STATE_PATH = "state.json"
RANDOM_DATA_GENERATION = 1
