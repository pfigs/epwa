#!/usr/bin/env bash

set -e

template_copy()
{
    local _TEMPLATE_PATH
    local _OUTPUT_PATH

    _TEMPLATE_PATH="${1}"
    _OUTPUT_PATH="${2}"

    if [ -f "${_OUTPUT_PATH}" ]
    then
        rm -f "${_OUTPUT_PATH}" "${_OUTPUT_PATH}.tmp"
    fi

    ( echo "cat <<EOF >${_OUTPUT_PATH}";
      cat "${_TEMPLATE_PATH}";
      echo "EOF";
    ) > "${_OUTPUT_PATH}.tmp"
    # shellcheck source=/dev/null
    . "${_OUTPUT_PATH}.tmp"
    rm "${_OUTPUT_PATH}.tmp"
}

_main()
{
   local _TEMPLATE_PATH
   local _OUTPUT_PATH

    _TEMPLATE_PATH="${1}"
    _OUTPUT_PATH="${2}"

   template_copy "${_TEMPLATE_PATH}" "${_OUTPUT_PATH}"
}

_main "${@}"
