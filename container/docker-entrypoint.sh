#!/usr/bin/env bash

set -e

SETTINGS_OUTPUT=settings.json

_nev()
{
    echo "generating settings file to: ${SETTINGS_OUTPUT}"
    rm -vf "${SETTINGS_OUTPUT:?}"
    ./template_copy.sh settings.template "${SETTINGS_OUTPUT}"
    echo "Just for illustration purposes"
    cat "${SETTINGS_OUTPUT}"
}

_main()
{
   if [[ "$1" == "nev_msg_agent" ]]
   then
       _nev "$@"
   fi
   echo "executing: ${*}"
   exec "$@"
}

_main "$@"
