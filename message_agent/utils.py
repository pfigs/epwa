"""
    Utils
    =====

    An assortment of methods and utility classes

"""


import datetime
import enum
import json
import uuid
import random
from .flags import StateFlags


class Serializer(json.JSONEncoder):
    """ Specialization of the JSON encoder """

    # As instructed by the python docs, likely a pylint issue
    # pylint: disable=method-hidden, arguments-differ
    def default(self, obj):
        """ Handles serialization of common types """
        if isinstance(obj, datetime.datetime):
            return f"{datetime.datetime.isoformat(obj)}Z"
        if isinstance(obj, enum.Enum):
            return obj.value
        return super().default(obj)


def read_jfile(path):
    """ Loads and returns a JSON file """
    with open(path, "r") as inputfile:
        data = json.load(inputfile)
    return data


def generate_data(identifier=None, nb_schedules=5, flags=None):
    """
        Generates random sample data with a fixed id or random one

        Args:

        identifier (string): device identifier
        nb_schedules (int): number of schedules to create
    """

    if identifier is None:
        identifier = str(uuid.uuid1())

    if not flags:
        flags = list(StateFlags)

    data = dict(
        id=identifier, name=__name__, flags=random.choice(flags).value, schedules=dict()
    )

    for _ in range(0, nb_schedules):
        data["schedules"][str(uuid.uuid4())] = dict(
            flags=random.choice(flags).value,
            begin=f"{datetime.datetime.now().isoformat()}Z",
            end=f"{(datetime.datetime.now() + datetime.timedelta(hours=3)).isoformat()}Z",
        )

    return data
