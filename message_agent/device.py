"""
    Device
    ======

    This file contains interfaces that handle how a device
    configuration is acquired and how it affects the
    current state of the device.

"""

import json
import queue
import multiprocessing

from .connection import MQTT
from .schedule import Schedule
from .flags import StateFlags
from .utils import Serializer
from .notify import Notify


class DeviceSerializer(Serializer):
    """ Json serializer for Device """

    # As instructed by the python docs, likely a pylint issue
    # pylint: disable=method-hidden
    def default(self, obj):
        """ Overloads base default to handle custom object serialization """
        if isinstance(obj, Schedule):
            return obj.to_dict()
        return super().default(obj)


class Device:
    """
    Device

    The Device class represent a physical edge component identified
    by its id.

    The edge component contains a state, which is initialize from a
    file as a last case resort. Its initial configuration or its last
    correct configuration ought to be pulled from MQTT as it connects,
    eg, through the means of a persistent topic (device/message/<id>)

    """

    def __init__(self, state, settings):

        super().__init__()

        self._state = state
        self._update_state()

        self._manager = multiprocessing.Manager()
        self._exit_signal = self._manager.Event()
        self._request = self._manager.Queue()
        self._event = self._manager.Queue()

        self.connection = MQTT(
            settings=settings["mqtt"],
            request=self._request,
            event=self._event,
            identifier=self._state["id"],
            exit_signal=self._exit_signal,
        )

        self.connection.on_connect_message = self.on_connect_message()
        self.connection.last_will_message = self.last_will_message()

    @property
    def state(self):
        """ Return the current state of the device """
        return self._state

    @property
    def identifier(self):
        """ Returns the device identifier """
        return self._state["id"]

    @property
    def name(self):
        """ Returns the device name """
        return self._state["name"]

    @property
    def flags(self):
        """ Returns the current device flag """
        return self._state["flags"].value

    @property
    def schedules(self):
        """ Returns the known schedules """
        return self._state["schedules"]

    @state.setter
    def state(self, value):
        """
            Sets the state of the device

            Args:
                value (dict): the state as a dictionary
        """
        self._state = value
        self._update_state()

    def _update_state(self):
        """ Updates and builds the state flags and schedules objects """
        self._state["flags"] = StateFlags(self.state["flags"])
        for identifier, content in self.schedules.items():
            self._state["schedules"][identifier] = Schedule(identifier, content)

    @flags.setter
    def flags(self, value):
        """ Sets the device's flags """
        print(f"Device: updating flag {self.flags} -> {value}")
        self._state["flags"] = StateFlags(value)

    @schedules.setter
    def schedules(self, value):
        """ Updates the current device schedules

            Args:
                value (dict): the schedule as a dictionary
        """
        active_schedules = set(self.schedules.keys())
        next_schedules = set(value.keys())
        remove_schedules = active_schedules - next_schedules

        # remove the schedules in the remove_schedules set
        for identifier in remove_schedules:
            Notify.delete(identifier)
            try:
                del self.schedules[identifier]
            except KeyError:
                continue

        # Update or add the new schedules
        for identifier, content in value.items():
            if identifier in self.schedules:
                print(f"Device: updating existing schedule {identifier}")
                self._state["schedules"][identifier].update(content)
                Notify.update(identifier, content)
                continue
            else:
                print(f"Device: creating new schedule {identifier}")
                Notify.create(identifier, content)

            self._state["schedules"][identifier] = Schedule(identifier, content)

    def start(self, timeout=1):
        """ Starts the connection loop and waits for an exit request """
        worker = multiprocessing.Process(target=self.connection.start)
        worker.start()

        while not self._exit_signal.is_set():
            try:
                message = self._event.get(timeout=timeout)
            except queue.Empty:
                continue
            print(f"Device: received message: {message}")
            self.update(message)
            if not worker.is_alive():
                break

        if worker.is_alive():
            worker.terminate()
            worker.join(timeout=timeout)

    def update(self, message):
        """ Takes a message and updates the DEVICE's state"""
        self.flags = message["flags"]
        self.schedules = message["schedules"]
        return True

    def on_connect_message(self):
        """ Defines the topic and the content to publish when the device connects """
        event = dict()
        event["id"] = self.identifier
        event["type"] = {StateFlags.ONLINE.name: StateFlags.ONLINE.value}
        event["message"] = f"{self.identifier} is now connected"
        event["state"] = self.to_dict()
        return json.dumps(event, cls=DeviceSerializer)

    def last_will_message(self):
        """ Defines the topic and the content to publish when the connection is lost """
        event = dict()
        event["id"] = self.identifier
        event["type"] = {StateFlags.OFFLINE.name: StateFlags.OFFLINE.value}
        event["message"] = f"{self.identifier} has exited"
        event["state"] = self.to_dict()
        return json.dumps(event, cls=DeviceSerializer)

    def shutdown(self):
        """ Terminates its execution """
        if not self._exit_signal.is_set():
            self._exit_signal.set()

    def to_dict(self):
        """ Returns a dictionary representation of device """
        return dict(
            name=self.name,
            id=self.identifier,
            flags=self.flags,
            schedules=self.schedules,
        )

    def __str__(self):
        return json.dumps(self.to_dict(), indent=4, cls=DeviceSerializer)
