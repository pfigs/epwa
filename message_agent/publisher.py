""" Publisher """
import multiprocessing
import json
from .connection import MQTT


class Publisher(MQTT):
    """
    Publisher

    Simple class to handle the publishing of messages towards the broker.

    """

    def __init__(self, settings):

        self._manager = multiprocessing.Manager()
        super().__init__(
            settings=settings["mqtt"],
            request=self._manager.Queue(),
            event=self._manager.Queue(),
            exit_signal=self._manager.Event(),
        )

        self._topics = {f"device/event/#": self.on_message_cb()}
        self._client.on_publish = self.on_publish()

    @staticmethod
    def on_message_cb():
        """ Callback to print events raised by the devices. """

        def on_message(client, userdata, message):
            try:
                payload = json.loads(message.payload)
            except json.decoder.JSONDecodeError:
                return
            print(f"Publisher: event observed: {payload}")

        return on_message

    @staticmethod
    def on_publish():
        """ Callback to print events raised by the devices. """

        def on_message(client, userdata, mid):

            print(f"Publisher: published {mid}")

        return on_message
