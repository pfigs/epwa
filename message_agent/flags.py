""" Flags

    Contains the set of known and valid flags

"""

import enum


class StateFlags(enum.Enum):
    """
        StateFlags
        Enum class contain all the available state flags
    """

    UPDATE = 1
    UPLOAD = 2
    RECORD = 3
    STOP = 4
    FLAG_123 = 123
    FLAG_456 = 456
    FLAG_789 = 789
    OFFLINE = 800
    ONLINE = 801
