"""

    Connection module
    =================

    This module contains a generic interface to implement
    by the underlying transport interfaces.

"""
import time


class Connection:
    """
    Connection

    Handles the storage and creation of a transport
    session from where to pull or receive configuration.
    """

    QUEUE_MAX_SIZE = 100
    TIMEOUT = 1

    def __init__(
        self, settings=None, request=None, event=None, identifier=None, exit_signal=None
    ):
        super().__init__()
        if settings is None:
            raise ValueError("Please provide connection settings")
        self._settings = settings
        self._identifier = identifier
        self._type = None
        self._request = request
        self._event = event
        self._timeout = Connection.TIMEOUT
        self._exit_signal = exit_signal

    @property
    def settings(self):
        """ Returns the connection settings """
        return self._settings

    @property
    def identifier(self):
        """ Returns the connection settings """
        return self._identifier

    def connect(self):
        """ Duck type for the connection method (transport specific) """
        raise NotImplementedError

    def start(self, block=True):
        """ Generic loop to monitor the exit signal """
        while not self._exit_signal.is_set():
            time.sleep(self._timeout)
            # check if there are messages in device queue
        self.shutdown()

    def shutdown(self):
        """ Signals the exit if not set yet """
        if not self._exit_signal.is_set():
            self._exit_signal.set()

    def get(self):
        """ Retrieves messages from the input queue """
        raise NotImplementedError

    def put(self, message):
        """ Puts messages in the output queue """
        if self._event.qsize() < self.QUEUE_MAX_SIZE:
            self._event.put(message)
