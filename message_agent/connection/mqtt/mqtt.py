"""

    MQTT module
    ===========

    This module contains the MQTT specific interfaces and methods.

"""
import json
import paho.mqtt.client as paho_mqtt
from ..connection import Connection


class MQTT(Connection):
    """
    MQTT

    Interface to establish a MQTT connection
    towards a remote server

    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._type = "MQTT"

        clean_sesion = True
        if self.identifier:
            clean_sesion = False

        self._client = paho_mqtt.Client(
            client_id=self.identifier, clean_session=clean_sesion
        )
        self._client.on_connect = self.on_connect_cb()
        self._topics = {
            f"device/configuration/{self.identifier}": self.on_configuration_cb(),
            f"device/message/{self.identifier}": self.on_message_cb(),
        }

        self._last_will_topic = f"device/event/{self.identifier}"
        self._on_connect_topic = f"device/event/{self.identifier}"
        self.on_connect_message = None
        self.last_will_message = None

    def _get_field(self, name):
        if name not in self._settings:
            raise ValueError(f"Missing {name} for MQTT connection")
        return self._settings[name]

    @property
    def host(self):
        """ Returns the broker's hostname to connect to """
        return self._get_field("host")

    @property
    def port(self):
        """ Returns the broker's port to use """
        return self._get_field("port")

    @property
    def username(self):
        """ Returns the user's name to use in the connection authentication """
        try:
            return self._get_field("username")
        except KeyError:
            return None

    @property
    def password(self):
        """ Returns the user's password to use in the connection authentication """
        try:
            return self._get_field("password")
        except KeyError:
            return None

    @property
    def topics(self):
        """ Returns the MQTT topics to/currently subscribed to """
        return self._topics

    @topics.setter
    def topics(self, value):
        self._topics = value

    def subscribe(self):
        """ Subscribes to the topics and adds callback to handle them """
        if self._topics:
            for topic, callback in self._topics.items():
                self._client.message_callback_add(topic, callback)
                self._client.subscribe(topic, 0)

    def start(self, block=True):
        """ Main loop
            Connects to the target broker, subscribes to the messages
            and starts the main loop thread.
        """

        if self.username and self.password:
            self._client.username_pw_set(self.username, self.password)

        self.will_set(
            self._last_will_topic, payload=self.last_will_message, qos=1, retain=True
        )
        self._client.connect(host=self.host, port=self.port)
        self._client.loop_start()

        if block:
            super().start(block=block)

    def on_connect_cb(self):
        """ Returns a callback to handle the connection establishment """

        def on_connect(client, userdata, flags, rc):
            """
            Callback that is called when connection to MQTT has succeeded.
            Here, we're subscribing to the incoming topics.
            Args:
               client (object): The MQTT client instance for this callback;
               userdata (object): The private user data;
               flags (list): A list of flags;
               rc (int): The connection result.
            """
            if rc != 0:
                self.shutdown()
                return

            self.subscribe()

            if self.on_connect_message:
                self._client.publish(
                    self._on_connect_topic, self.on_connect_message, qos=1, retain=True
                )
                print(f"MQTT connection is ready: Hello! {self.on_connect_message}")

        return on_connect

    def on_configuration_cb(self):
        """ Callback to handle an incoming configuration.

            The message is acquired on a parallel process
            and sent to the internal queue, where it is
            processed further.

            It is discarded if the json is invalid or the
            recipient is incorrect.
        """

        def on_configuration(client, userdata, message):
            try:
                payload = json.loads(message.payload)
                if self.identifier == payload["id"]:
                    self.put(payload)
                else:
                    print("Device: discarding message as it is not for me")
            except json.decoder.JSONDecodeError:
                print("Incorrect encoding")
                pass

        return on_configuration

    def on_message_cb(self):
        """ Callback to handle an incoming message.

            The message is acquired on a parallel process
            and sent to the internal queue, where it is
            processed further.

            It is discarded if the json is invalid or the
            recipient is incorrect.
        """

        def on_message(client, userdata, message):

            try:
                payload = json.loads(message.payload)
            except json.decoder.JSONDecodeError:
                return

            try:
                print(f"Device: received message: {payload}")
                if "OFFLINE" in payload["type"]:
                    self.shutdown()
            except KeyError:
                pass
            except TypeError:
                print(f"Device: received message with incorrect message type")

        return on_message

    def shutdown(self):
        self._client.disconnect()
        self._client.loop_stop()
        super().shutdown()

    def __getattr__(self, name):
        return getattr(self._client, name)
