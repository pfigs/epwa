""" Notify """


class Notify:
    """
    Notify

    This is a wrapper calls that would be able
    to notify an external daemon about new
    configuration events.

    For example, communication with systemd units
    should be done through dbus as that is the
    communication bus used by systemd.

    Quick workarounds would involve the system's
    shell to do the work for us, but that would
    increase the attack vector.
    """

    def __init__(self):
        super().__init__()

    @staticmethod
    def delete(identifier):
        """ Notifies an external party about a deletion event """
        print(f"Notify: deleting {identifier}")

    @staticmethod
    def update(identifier, content):
        """ Notifies an external party about an update event """
        print(f"Notify: updating {identifier}")

    @staticmethod
    def create(identifier, content):
        """ Notifies an external party about a creation event """
        print(f"Notify: creating {identifier}")
