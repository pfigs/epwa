""" NEV configuration agent """

import argparse
import json
from .device import Device


def mqtt_agent():
    """ The MQTT configuration agent """

    parser = argparse.ArgumentParser(description="Device configuration")
    parser.add_argument(
        "--state",
        type=str,
        required=False,
        default="state.json",
        help="path to current state",
    )
    parser.add_argument(
        "--connection",
        type=str,
        required=True,
        default="settings.json",
        help="path to connection settings",
    )

    args = parser.parse_args()

    with open(args.state, "r") as inputfile:
        state = json.load(inputfile)

    with open(args.connection, "r") as inputfile:
        settings = json.load(inputfile)

    print("Initializing device")
    print(f"reading state from: {args.state}")
    print(f"reading connection settings from: {args.connection}")
    device = Device(state=state, settings=settings)
    print(device)
    try:
        device.start()
    except KeyboardInterrupt:
        print(device)


if __name__ == "__main__":
    mqtt_agent()
