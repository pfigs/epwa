""" SChedule """
import datetime
import json
from .flags import StateFlags
from .utils import Serializer


class ScheduleSerializer(Serializer):
    """ Json serializer for Device """

    # As instructed by the python docs, likely a pylint issue
    # pylint: disable=method-hidden
    def default(self, obj):
        """ Overloads base default to handle custom object serialization """
        return super().default(obj)


class Schedule:
    """Schedule"""

    def __init__(self, identifier, content):
        super().__init__()
        self._identifier = identifier
        self._flags = StateFlags(content["flags"])
        self._begin = self.convert_time(content["begin"])
        self._end = self.convert_time(content["end"])

    @staticmethod
    def convert_time(date):
        """ Converts the isoformat string to datetime """
        return datetime.datetime.fromisoformat(date.replace("Z", ""))

    @property
    def identifier(self):
        """ Return the schedule's identifier """
        return self._identifier

    @property
    def flags(self):
        """ Returns the schedule's flag """
        return self._flags.value

    @property
    def begin(self):
        """ Returns the date when the schedule will/shall start """
        return self._begin

    @property
    def end(self):
        """ Returns the date when the schedule will/shall stop """
        return self._end

    def update(self, content):
        """ Updates the schedule content

            Args:
                content (dict): a dictionary with the new schedule content
        """

        self._flags = StateFlags(content["flags"])
        begin = self.convert_time(content["begin"])
        end = self.convert_time(content["end"])

        if end < begin:
            return False

        if begin != self.begin:
            self.begin = begin

        if end != self.end:
            self.end = end

        return True

    def to_dict(self):
        """ Builds a dictionary representation of the schedule """
        return {
            self._identifier: dict(flags=self._flags, begin=self._begin, end=self._end)
        }

    def __str__(self):
        return json.dumps(self.to_dict(), cls=ScheduleSerializer)
